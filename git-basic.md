# What is version control
 > Verson control is system that
 records changes to a file or set of file over time so that we cna recall specifc version later;
 
# Why is version control used?
 >We can recall the specifc version later;
 
# What is git?
 >Git is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows
 
# What does the staging area mean?
 >Staging is a step before the commit process in git. That is, a commit in git is performed in two steps: staging and actual commit. As long as a changeset is in the staging area, git allows you to edit it as you like 
 
# What is a branch? 
 >branch in Git is simply a lightweight movable pointer to one of these commits. The default branch name in Git is master. As you initially make commits, you're given a master branch that points to the last commit you made
 
# What is a remote
 >remote in Git is a common repository that all team members use to exchange their changes. In most cases, such a remote repository is stored on a code hosting service like GitHub or on an internal server.
